<%--
  Created by IntelliJ IDEA.
  User: rakibul.hasan
  Date: 3/25/20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>

<head>
    <title>$Title$</title>
</head>

<body>
    <form:form action="/login" method="post" modelAttribute="user">
        <form:errors path="userName" cssStyle="color: red"/>
        <br/>

        User Name <form:input type="text" path="userName"/>
        <br/>

        Password <form:input type="password" path="password"/>
        <br/>

        <form:button>Login</form:button>
    </form:form>
</body>

</html>
