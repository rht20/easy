<%--
  User: rakibul.hasan
  Date: 3/26/20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>

<head>
    <title>Title</title>
</head>

<body>
<form:form action="/registerCompany" method="post" modelAttribute="company">
    <form:errors path="user.userName" cssStyle="color: red" element="li"/>
    User Name <form:input type="text" path="user.userName"/>
    <br/>
    <br/>

    <form:errors path="user.password" cssStyle="color: red" element="li"/>
    Password <form:input type="password" path="user.password"/>
    <br/>
    <br/>

    <form:errors path="companyName" cssStyle="color: red" element="li"/>
    Company Name <form:input type="text" path="companyName"/>
    <br/>
    <br/>

    <form:errors path="email" cssStyle="color: red" element="li"/>
    Email Address <form:input type="text" path="email"/>
    <br/>
    <br/>

    <form:errors path="phone" cssStyle="color: red" element="li"/>
    Phone Number <form:input type="text" path="phone"/>
    <br/>
    <br/>

    <form:errors path="location" cssStyle="color: red" element="li"/>
    Office Location <form:input type="text" path="location"/>
    <br/>
    <br/>

    <form:button>Register</form:button>
</form:form>
</body>

</html>
