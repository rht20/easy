<%--
  User: rakibul.hasan
  Date: 3/25/20
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>

<head>
    <title>Title</title>
</head>

<body>
    <form:form action="/admin/profile/edit" method="post" modelAttribute="user">
        <form:errors path="password" cssStyle="color: red"/>
        <br/>

        New Password <form:input type="password" path="password"/>
        <br/>

        <form:button>Update</form:button>
    </form:form>
</body>

</html>
