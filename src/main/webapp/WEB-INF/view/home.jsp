<%--
  Created by IntelliJ IDEA.
  User: rakibul.hasan
  Date: 3/25/20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <title>$Title$</title>
</head>

<body>
    <c:forEach items="${dashboard}" var="item">
        <a href="${item["action"]}">
            ${item["name"]}
        </a>

        <br/>
    </c:forEach>
</body>

</html>
