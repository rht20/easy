package net.therap.easyWay.controller;

import net.therap.easyWay.model.User;
import net.therap.easyWay.service.UserService;
import net.therap.easyWay.validator.LoginFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

/**
 * @author rakibul.hasan
 * @since 3/25/20
 */
@Controller
public class AuthController {

    @Autowired
    UserService userService;

    @Autowired
    LoginFormValidator loginFormValidator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(loginFormValidator);
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(ModelMap modelMap) {

        User user = new User();
        modelMap.put("user", user);

        return "loginForm";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@Validated @ModelAttribute(value = "user") User user,
                        Errors errors,
                        ModelMap modelMap,
                        HttpSession session) {

        if (errors.hasErrors()) {
            modelMap.put("user", user);
            return "loginForm";
        }

        user = userService.getUser(user);
        System.out.println(user);

        session.setAttribute("userId", user.getId());
        session.setAttribute("userType", user.getUserType().name());

        return "redirect:/";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpSession session) {
        session.invalidate();

        return "redirect:/";
    }
}
