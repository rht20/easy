package net.therap.easyWay.controller;

import net.therap.easyWay.model.Company;
import net.therap.easyWay.model.User;
import net.therap.easyWay.service.CompanyService;
import net.therap.easyWay.service.UserService;
import net.therap.easyWay.validator.AdminProfileEditFormValidator;
import net.therap.easyWay.validator.CompanyRegiFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

/**
 * @author rakibul.hasan
 * @since 3/25/20
 */
@Controller
public class AdminController {

    @Autowired
    UserService userService;

    @Autowired
    CompanyService companyService;

    @Autowired
    AdminProfileEditFormValidator adminProfileEditFormValidator;

    @Autowired
    CompanyRegiFormValidator companyRegiFormValidator;

    @InitBinder("user")
    private void initUserBinder(WebDataBinder binder) {
        binder.setValidator(adminProfileEditFormValidator);
    }

    @InitBinder("company")
    private void initCompanyBinder(WebDataBinder binder) {
        binder.setValidator(companyRegiFormValidator);
    }

    @RequestMapping(value = "/admin/profile/edit", method = RequestMethod.GET)
    public String editProfile(ModelMap modelMap) {

        User user = new User();
        modelMap.put("user", user);

        return "adminProfileEditForm";
    }

    @RequestMapping(value = "/admin/profile/edit", method = RequestMethod.POST)
    public String editProfile(@Validated @ModelAttribute(value = "user") User user,
                              Errors errors,
                              ModelMap modelMap,
                              HttpSession session) {

        if (errors.hasErrors()) {
            return "adminProfileEditForm";
        }

        user.setId((Integer) session.getAttribute("userId"));
        userService.updateUser(user);

        return "redirect:/";
    }

    @RequestMapping(value = "/registerCompany", method = RequestMethod.GET)
    public String registerCompany(ModelMap modelMap) {

        Company company = new Company();
        User user = new User();
        company.setUser(user);

        modelMap.put("company", company);

        return "companyRegistrationForm";
    }

    @RequestMapping(value = "/registerCompany", method = RequestMethod.POST)
    public String registerCompany(@Validated @ModelAttribute(value = "company") Company company,
                                  Errors errors) {

        if (errors.hasErrors()) {
            return "companyRegistrationForm";
        }

        companyService.addCompany(company);

        return "redirect:/";
    }
}
