package net.therap.easyWay.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author rakibul.hasan
 * @since 3/25/20
 */
@Controller
public class HomePageController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(HttpSession session, ModelMap modelMap) {

        if (session.getAttribute("userType") == null) {
            setCommonDashboard(modelMap);

        } else if (session.getAttribute("userType").equals("ADMIN")) {
            setAdminDashboard(modelMap);

        } else if (session.getAttribute("userType").equals("COMPANY")) {
            setCompanyDashboard(modelMap);

        } else if (session.getAttribute("userType").equals("PASSENGER")) {
            setPassengerDashboard(modelMap);
        }

        return "home";
    }

    private void setAdminDashboard(ModelMap modelMap) {
        List<Map> dashboard = new ArrayList<>();

        Map map = new HashMap();
        map.put("name", "Edit Profile");
        map.put("action", "/admin/profile/edit");
        dashboard.add(map);

        map = new HashMap();
        map.put("name", "Register Company");
        map.put("action", "/registerCompany");
        dashboard.add(map);

        map = new HashMap();
        map.put("name", "Logout");
        map.put("action", "/logout");
        dashboard.add(map);

        modelMap.put("dashboard", dashboard);
    }

    private void setCompanyDashboard(ModelMap modelMap) {

    }

    private void setPassengerDashboard(ModelMap modelMap) {

    }

    private void setCommonDashboard(ModelMap modelMap) {
        List<Map> dashboard = new ArrayList<>();

        Map map = new HashMap();
        map.put("name", "Login");
        map.put("action", "/login");
        dashboard.add(map);

        map = new HashMap();
        map.put("name", "Register");
        map.put("action", "/register");
        dashboard.add(map);

        modelMap.put("dashboard", dashboard);
    }
}
