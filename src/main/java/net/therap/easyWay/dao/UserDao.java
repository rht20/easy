package net.therap.easyWay.dao;

import net.therap.easyWay.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 3/25/20
 */
@Repository
public class UserDao {

    @PersistenceContext
    private EntityManager em;

    public User getUserById(int id) {
        return em.find(User.class, id);
    }

    public User getUserByUserName(String userName) {
        TypedQuery<User> query = em.createQuery("SELECT U FROM User AS U WHERE U.userName = :userName", User.class)
                .setParameter("userName", userName);

        List<User> users = query.getResultList();

        return users.isEmpty() ? null : users.get(0);
    }

    public User getUserByUserNameAndPassword(String userName, String password) {
        TypedQuery<User> query = em.createQuery("SELECT U FROM User AS U WHERE U.userName = :userName AND U.password = :password", User.class)
                .setParameter("userName", userName)
                .setParameter("password", password);

        List<User> users = query.getResultList();

        return users.isEmpty() ? null : users.get(0);
    }

    @Transactional
    public void addUser(User user) {
        em.persist(user);
        em.flush();
    }

    @Transactional
    public void updateUser(User user) {
        em.merge(user);
        em.flush();
    }
}
