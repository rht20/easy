package net.therap.easyWay.dao;

import net.therap.easyWay.model.Company;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * @author rakibul.hasan
 * @since 3/26/20
 */
@Repository
public class CompanyDao {

    @PersistenceContext
    EntityManager em;

    @Transactional
    public void addCompany(Company company) {
        em.persist(company);
        em.flush();
    }
}
