package net.therap.easyWay.validator;

import net.therap.easyWay.model.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author rakibul.hasan
 * @since 3/25/20
 */
@Component
public class AdminProfileEditFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        System.out.println("validator");
        User user = (User) target;

        if (user.getPassword().equals("")) {
            errors.rejectValue("password", "", "Password can't be empty.");
        }
    }
}
