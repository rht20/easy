package net.therap.easyWay.validator;

import net.therap.easyWay.model.Company;
import net.therap.easyWay.model.User;
import net.therap.easyWay.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author rakibul.hasan
 * @since 3/26/20
 */
@Component
public class CompanyRegiFormValidator implements Validator {

    @Autowired
    UserService userService;

    @Autowired
    ContactInfoValidator contactInfoValidator;

    @Override
    public boolean supports(Class<?> clazz) {
        return Company.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Company company = (Company) target;
        User user = company.getUser();

        if (user.getUserName().equals("")) {
            errors.rejectValue("user.userName", "", "Please provide user name.");

        } else if (userService.checkUserByUserName(user.getUserName())) {
            errors.rejectValue("user.userName", "", "User name exists.");
        }

        if (user.getPassword().equals("")) {
            errors.rejectValue("user.password", "", "Please provide password.");
        }

        if (company.getCompanyName().equals("")) {
            errors.rejectValue("companyName", "", "Please provide company name.");
        }

        if (company.getEmail().equals("")) {
            errors.rejectValue("email", "", "Please provide email address.");

        } else if (!contactInfoValidator.isValidEmail(company.getEmail())) {
            errors.rejectValue("email", "", "Email address is not valid.");
        }

        if (company.getPhone().equals("")) {
            errors.rejectValue("phone", "", "Please provide phone number.");

        } else if (!contactInfoValidator.isValidPhoneNumber(company.getPhone())) {
            errors.rejectValue("phone", "", "Phone number is not valid.");
        }

        if (company.getLocation().equals("")) {
            errors.rejectValue("location", "", "Please provide office location.");
        }
    }
}
