package net.therap.easyWay.validator;

import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

/**
 * @author rakibul.hasan
 * @since 3/26/20
 */
@Component
public class ContactInfoValidator {

    public boolean isValidEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$";

        Pattern pattern = Pattern.compile(emailRegex);

        return pattern.matcher(email).matches();
    }

    public boolean isValidPhoneNumber(String phone) {
        Pattern pattern = Pattern.compile("[0-9]{11}");
        return pattern.matcher(phone).matches();
    }
}
