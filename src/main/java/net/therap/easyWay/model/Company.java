package net.therap.easyWay.model;

import javax.persistence.*;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 3/24/20
 */
@Entity
@Table(name = "company")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    private User user;

    @Column(name = "company_name")
    private String companyName;

    private String email;
    private String phone;
    private String location;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Bus> buses;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Schedule> schedules;

    @OneToMany(fetch = FetchType.EAGER)
    private List<BookingHistory> bookingHistories;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<Bus> getBuses() {
        return buses;
    }

    public void setBuses(List<Bus> buses) {
        this.buses = buses;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }

    public List<BookingHistory> getBookingHistories() {
        return bookingHistories;
    }

    public void setBookingHistories(List<BookingHistory> bookingHistories) {
        this.bookingHistories = bookingHistories;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", user=" + user +
                ", companyName='" + companyName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", location='" + location + '\'' +
                ", buses=" + buses +
                ", schedules=" + schedules +
                ", bookingHistories=" + bookingHistories +
                '}';
    }
}
