package net.therap.easyWay.model;

/**
 * @author rakibul.hasan
 * @since 3/24/20
 */
public enum UserType {

    ADMIN, COMPANY, PASSENGER
}
