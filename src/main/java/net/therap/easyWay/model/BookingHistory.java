package net.therap.easyWay.model;

import javax.persistence.*;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 3/24/20
 */
@Entity
@Table(name = "booking_history")
public class BookingHistory {

    @Id
    @GeneratedValue
    private int id;

    @OneToOne
    private Passenger passenger;

    @OneToOne
    private Company company;

    @OneToOne
    private Schedule schedule;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Seat> seats;

    @Column(name = "total_price")
    private double totalPrice;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
