package net.therap.easyWay.model;

/**
 * @author rakibul.hasan
 * @since 3/25/20
 */
public enum Gender {

    MALE, FEMALE, OTHER
}
