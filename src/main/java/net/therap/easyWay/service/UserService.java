package net.therap.easyWay.service;

import net.therap.easyWay.dao.UserDao;
import net.therap.easyWay.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author rakibul.hasan
 * @since 3/25/20
 */
@Component
public class UserService {

    @Autowired
    UserDao userDao;

    public User getUser(User user) {
        return userDao.getUserByUserNameAndPassword(user.getUserName(), user.getPassword());
    }

    public User getUserById(int id) {
        return userDao.getUserById(id);
    }

    public boolean checkUser(User user) {
        return userDao.getUserByUserNameAndPassword(user.getUserName(), user.getPassword()) != null;
    }

    public boolean checkUserByUserName(String userName) {
        return userDao.getUserByUserName(userName) != null;
    }

    public void addUser(User user) {
        userDao.addUser(user);
    }

    public void updateUser(User user) {
        User cur = userDao.getUserById(user.getId());
        cur.setPassword(user.getPassword());

        userDao.updateUser(cur);
    }
}
