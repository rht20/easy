package net.therap.easyWay.service;

import net.therap.easyWay.dao.CompanyDao;
import net.therap.easyWay.model.Company;
import net.therap.easyWay.model.User;
import net.therap.easyWay.model.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * @author rakibul.hasan
 * @since 3/26/20
 */
@Component
public class CompanyService {

    @Autowired
    CompanyDao companyDao;

    @Autowired
    UserService userService;

    public void addCompany(Company company) {
        User user = company.getUser();

        for (UserType userType : UserType.values()) {
            if (userType.name().equals("COMPANY")) {
                user.setUserType(userType);
                break;
            }
        }
        System.out.println(user);
        userService.addUser(user);

        user = userService.getUser(user);

        company.setUser(user);
        company.setBuses(new ArrayList<>());
        company.setSchedules(new ArrayList<>());
        company.setBookingHistories(new ArrayList<>());

        companyDao.addCompany(company);
    }
}
